[TOC]
### Shell 基本运算符
shell支持多种运算符：
- 算数运算符
- 关系运算符
- 布尔运算符
- 字符串运算符
- 文件测试运算符

原生bash不支持简单的数学运算，但是可以通过其他命令来实现，例如 awk 和 expr，expr 最常用。

expr 是一款表达式计算工具，使用它能完成表达式的求值操作。

例如，两个数相加(注意使用的是反引号 ` 而不是单引号 ')：
```shell
#########################################################################
# File Name: cal.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月26日 星期日 00时16分36秒
#########################################################################
#!/bin/bash

# Author: lsy
val=`expr 1 + 2`
echo "两数相加：$val"
```
##### 算术运算符
下表列出了常用的算术运算符，假定变量 a 为 10，变量 b 为 20：

| 运算符 | 说明 |举例|
|--------|--------|------|
|+|加法|\`expa $a + $b\` :30|
|-|减法|\`expr $a - $b\` :-10|
|*|乘法|\`expr $a \* $b\` :200|
|/|除法|\`expr $a / $b\` :0|
|%|取余|\`expr $b % $a` 结果为 0。|
|=|赋值|a=$b 将把变量 b 的值赋给 a。|
|==|相等。用于比较两个数字，相同则返回 true。|[ $a == $b ] 返回 false。|
|!=|不相等。用于比较两个数字，不相同则返回 true。|[ $a != $b ] 返回 true。|
==注意==：条件表达式要放在方括号之间，并且要有空格，例如: [$a\=\=$b] 是错误的，必须写成 [ $a \=\= $b ]。
- 乘号(*)前边必须加反斜杠(\)才能实现乘法运算；
- 在 MAC 中 shell 的 expr 语法是：$((表达式))，此处表达式中的 "*" 不需要转义符号 "\" 。



##### 关系运算符

==关系运算符只支持数字，不支持字符串，除非字符串的值是数字。==

下表列出了常用的关系运算符，假定变量 a 为 10，变量 b 为 20：

|运算符   |	说明 |	举例|
|--------|--------|------|
|-eq |	检测两个数是否相等，相等返回 true。 |	[ $a -eq $b ] 返回 false。|
|-ne | 	检测两个数是否相等，不相等返回 true。| 	[ $a -ne $b ] 返回 true。|
|-gt |	检测左边的数是否大于右边的，如果是，则返回 true。 |	[ $a -gt $b ] 返回 false。|
|-lt |	检测左边的数是否小于右边的，如果是，则返回 true。 |	[ $a -lt $b ] 返回 true。|
|-ge |	检测左边的数是否大于等于右边的，如果是，则返回 true。 |	[ $a -ge $b ] 返回 false。|
|-le | 	检测左边的数是否小于等于右边的，如果是，则返回 true。 |	[ $a -le $b ] 返回 true。|

##### 布尔运算符

下表列出了常用的布尔运算符，假定变量 a 为 10，变量 b 为 20：

|运算符| 	说明 |	举例|
|--------|--------|------|
|! |	非运算，表达式为 true 则返回 false，否则返回 true。 	|[ ! false ] 返回 true。|
|-o |	或运算，有一个表达式为 true 则返回 true。 |	[ $a -lt 20 -o $b -gt 100 ] 返回 true。|
|-a| 	与运算，两个表达式都为 true 才返回 true。 |	[ $a -lt 20 -a $b -gt 100 ] 返回 false。|

##### 逻辑运算符

以下介绍 Shell 的逻辑运算符，假定变量 a 为 10，变量 b 为 20:

|运算符 |	说明 |	举例|
|--------|--------|------|
|&& |	逻辑的 AND |	[[ $a -lt 100 && $b -gt 100 ]] 返回 false|
|ll |	逻辑的 OR 	|[[ $a -lt 100 ll $b -gt 100 ]] 返回 true|

##### 字符串运算符
下表列出了常用的字符串运算符，假定变量 a 为 "abc"，变量 b 为 "efg"：

|运算符|说明|举例|
|-----|----|---|
|= |	检测两个字符串是否相等，相等返回 true。 |	[ $a = $b ] 返回 false。|
|!= |	检测两个字符串是否相等，不相等返回 true。| 	[ $a != $b ] 返回 true。|
|-z |	检测字符串长度是否为0，为0返回 true。 |	[ -z $a ] 返回 false。|
|-n |	检测字符串长度是否为0，不为0返回 true。| 	[ -n $a ] 返回 true。|
|str |	检测字符串是否为空，不为空返回 true。 |	[ $a ] 返回 true。|

例子：
```shell
#########################################################################
# File Name: str.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月26日 星期日 09时44分34秒
#########################################################################
#!/bin/bash

# Author: lsy
a="lsy"
b="lsm"
if [ $a = $b ]
then
	echo "a和b相等"
else
	echo "a和b不相同"
fi

if [ $a != $b ]
then
	echo "a和b不相同"
else
	echo "a和b相同"

fi
if [ -z $a ]
then
	echo "a为空"
else
	echo "a不为空"
fi

if [ -n $a ]
then
	echo "a不为空"
else
	echo "a为空"
fi

if [ $a ]
then
	echo "a不为空"
else
	echo "a为空"
fi
```
运行结果：
```shell
8opt$ ./str.sh
a和b不相同
a和b不相同
a不为空
a不为空
a不为空
```

##### 文件测试运算符

文件测试运算符常用于测试unix文件的各种属性

|操作符|说明|举例|
|------|------|------|
|-b file| 	检测文件是否是块设备文件，如果是，则返回 true。 |	[ -b $file ] 返回 false。|
|-c file |	检测文件是否是字符设备文件，如果是，则返回 true。 |	[ -c $file ] 返回 false。|
|-d file |	检测文件是否是目录，如果是，则返回 true。 	|[ -d $file ] 返回 false。|
|-f file |	检测文件是否是普通文件（既不是目录，也不是设备文件），如果是，则返回 true。 |	[ -f $file ] 返回 true。|
|-g file |	检测文件是否设置了 SGID 位，如果是，则返回 true。| 	[ -g $file ] 返回 false。|
|-k file 	|检测文件是否设置了粘着位(Sticky Bit)，如果是，则返回 true。 |	[ -k $file ] 返回 false。
|-p file |	检测文件是否是有名管道，如果是，则返回 true。 	|[ -p $file ] 返回 false。|
|-u file |	检测文件是否设置了 SUID 位，如果是，则返回 true。 |	[ -u $file ] 返回 false。|
|-r file |	检测文件是否可读，如果是，则返回 true。 |	[ -r $file ] 返回 true。|
|-w file |	检测文件是否可写，如果是，则返回 true。 	|[ -w $file ] 返回 true。|
|-x file |	检测文件是否可执行，如果是，则返回 true。 	|[ -x $file ] 返回 true。|
|-s file |	检测文件是否为空（文件大小是否大于0），不为空返回 true。 |	[ -s $file ] 返回 true。|
|-e file 	|检测文件（包括目录）是否存在，如果是，则返回 true。 	|[ -e $file ] 返回 true。|

例子：
```shell
#########################################################################
# File Name: file.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月26日 星期日 12时51分33秒
#########################################################################
#!/bin/bash

# Author: lsy
if [ ! -e $1 ]
then
	echo "$1 文件不存在"
	exit
fi

if [ -b $1 ]
then
	echo "$1 是块设备"
fi
if [ -c $1 ]
then
	echo "$1 是字符设备"
fi
if [ -d $1 ]
then
	echo "$1 是目录"
fi
if [ -f $1 ]
then
	echo "$1 是普通文件"
fi

if [ -g $1 ]
then
	echo ">$1 设置了SGID"
fi
if [ -k $1 ]
then
	echo "$1 设置了粘着位"
fi
if [ -p $1 ]
then
	echo "$1 有名管道"
fi
if [ -u $1 ]
then
	echo ">$1 设置了SUID位"
fi
if [ -r $1 ]
then
	echo "$1 文件可读"
fi
if [ -w $1 ]
then
	echo "$1 文件可写"
fi
if [ -x $1 ]
then
	echo "$1 文件可执行"
fi
if [ ! -s $1 ]
then
	echo >"$1 文件为空"
fi
```
执行结果：
```shell
34opt$ ./file.sh str.sh
str.sh 是普通文件
str.sh 文件可读
str.sh 文件可写
str.sh 文件可执行
```
