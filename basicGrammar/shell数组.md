[TOC]
##Shell 数组

数组中可以存放多个值。Bash Shell 只支持一维数组（不支持多维数组），初始化时不需要定义数组大小（与 PHP 类似）。
与大部分编程语言类似，数组元素的下标由0开始。
Shell 数组用括号来表示，元素用"空格"符号分割开，语法格式如下：
```shell
array={a,b,c,d,e,f}
```

#####下标定义的数组
```shell
array[0]=1
array[1]=2
array[2]=3
```
#####读取数组

读取数组元素值的一般格式是：
```shell
array[index]
```
例子：
```shell
#########################################################################
# File Name: array.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月25日 星期六 23时08分19秒
#########################################################################
#!/bin/bash

# Author: lsy
array[0]=0
array[1]=1
array[2]=2
echo "array[0]:${array[0]}"
echo "array[1]:${array[1]}"
echo "array[2]:${array[2]}"
````
结果：
```shell
56opt$ ./array.sh 
array[0]:0
array[1]:1
array[2]:2
```
#####获取数组中的所有元素
使用@ 或 * 可以获取数组中的所有元素，例如：
```shell
#########################################################################
# File Name: array.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月25日 星期六 23时08分19秒
#########################################################################
#!/bin/bash

# Author: lsy
array[0]=0
array[1]=1
array[2]=2
echo "array[0]:${array[0]}"
echo "array[1]:${array[1]}"
echo "array[2]:${array[2]}"
echo "数组所有元素（*）：${array[*]}"
echo "数组所有元素（@）：${array[@]}"
```
运行结果
```shell
array[0]:0
array[1]:1
array[2]:2
数组所有元素（*）：0 1 2
数组所有元素（@）：0 1 2
```

#####获取数组的长度

获取数组长度的方法与获取字符串长度的方法相同，例如：
```shell
#########################################################################
# File Name: array.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月25日 星期六 23时08分19秒
#########################################################################
#!/bin/bash

# Author: lsy
array[0]=0
array[1]=1
array[2]=2
echo "array[0]:${array[0]}"
echo "array[1]:${array[1]}"
echo "array[2]:${array[2]}"
echo "数组所有元素（*）：${array[*]}"
echo "数组所有元素（@）：${array[@]}"
echo "数组的长度:${#array[*]}"
echo "数组的长度:${#array[@]}"
```
运行结果：
```shell
array[0]:0
array[1]:1
array[2]:2
数组所有元素（*）：0 1 2
数组所有元素（@）：0 1 2
数组的长度:3
数组的长度:3
```