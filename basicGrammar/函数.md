[TOC]

###Shell 函数

linux shell 可以用户定义函数，然后在shell脚本中可以随便调用。

#####shell中函数的定义格式
```shell
[ function ] funname [()]
{
    action;
    [return int;]
}
```
说明：

- 可以带function fun() 定义，也可以直接fun() 定义,不带任何参数。
- 参数返回，可以显示加：return 返回，如果不加，将以最后一条命令运行结果，作为返回值。 return后跟数值n(0-255)


例子：
```shell
#########################################################################
# File Name: func1.sh
# company: GDOU
# mail: lsy476941913@live.com
# Created Time: 2017年03月26日 星期日 20时41分54秒
#########################################################################
#!/bin/bash

# Author: lsy

function func()
{
	echo "my first function";
	return 0;
}


echo "调用函数开始"
func
echo "调用函数结束"
```
运行结果：
```shell
6func$ ./func1.sh 
调用函数开始
my first function
调用函数结束
```

#####函数参数

在Shell中，调用函数时可以向其传递参数。在函数体内部，通过 $n 的形式来获取参数的值，例如，$1表示第一个参数，$2表示第二个参数...

带参数的函数示例：
```shell
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

funWithParam(){
    echo "第一个参数为 $1 !"
    echo "第二个参数为 $2 !"
    echo "第十个参数为 $10 !"
    echo "第十个参数为 ${10} !"
    echo "第十一个参数为 ${11} !"
    echo "参数总数有 $# 个!"
    echo "作为一个字符串输出所有参数 $* !"
}
funWithParam 1 2 3 4 5 6 7 8 9 34 73

```
输出结果：

```shell
第一个参数为 1 !
第二个参数为 2 !
第十个参数为 10 !
第十个参数为 34 !
第十一个参数为 73 !
参数总数有 11 个!
作为一个字符串输出所有参数 1 2 3 4 5 6 7 8 9 34 73 !
```

`注意`，$10 不能获取第十个参数，获取第十个参数需要${10}。当n>=10时，需要使用${n}来获取参数。