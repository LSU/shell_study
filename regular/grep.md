[TOC]

##grep搜索文本
####grep普通用法
######搜索文件中是否包含特定的模式
```shell
58html$ grep html index.html 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

</html>
```
######从stdin中读取文本流
```shell
59html$ echo -e "this an apple\nnew line" | grep an
this an apple
```
######对多个文本进行匹配
```shell
63html$ grep html index.html  hhh 
index.html:<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
index.html:<html xmlns="http://www.w3.org/1999/xhtml">
index.html:    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
index.html:  body, html {
index.html:                <tt>/var/www/html/index.html</tt>) before continuing to operate your HTTP server.
index.html:                <a href="http://httpd.apache.org/docs/2.4/mod/mod_userdir.html" rel="nofollow">public_html</a>
index.html:                The default Debian document root is <tt>/var/www/html</tt>. You
index.html:</html>
hhh:html
hhh:html
hhh:html
hhh:html
```
######用--color选项可以在输出行中着重标记出匹配到的单词
```shell
65html$ echo -e "this an apple\nnew line" | grep an --color=never
this an apple
```
`--color=never` 表示不用颜色显示

####如果grep使用正则表达式
######使用正则表达式需要添加`-E`选项或者使用egrep
例如：
```shell
72shell$ cat txt 
abcdefg
abcdefg
bcdefg
bcdefg
cbcdefg
73shell$ grep -E "^[b-z]+" txt 
bcdefg
bcdefg
cbcdefg
```

######只输出文本中匹配到的文本内容，使用-0选项
```shell
77shell$ echo "hello,this is a line." | grep -E "[l-z]lo+" -o
llo
```

######将匹配结果进行反转
```shell
89shell$ cat 99.txt 
1    2015-12-25    1.2366    1.2366
2    2015-12-24    1.2296    1.2296
3    2015-12-23    1.2324    1.2324
4    2015-12-22    1.2454    1.2454
5    2015-12-21    1.2351    1.2351
6    2015-12-18    1.2218    1.2218
7    2015-12-17    1.2247    1.2247
8    2015-12-16    1.1965    1.1965
9    2015-12-15    1.1906    1.1906 
90shell$ grep -v 1.2366  99.txt 
2    2015-12-24    1.2296    1.2296
3    2015-12-23    1.2324    1.2324
4    2015-12-22    1.2454    1.2454
5    2015-12-21    1.2351    1.2351
6    2015-12-18    1.2218    1.2218
7    2015-12-17    1.2247    1.2247
8    2015-12-16    1.1965    1.1965
9    2015-12-15    1.1906    1.1906
```
######统计文件或者文本包含匹配字符串的行数
```shell
91shell$ grep -v -c 1.2366  99.txt 
8
```
此做法只匹配行数，不统计匹配次数
######要匹配出现的数量
```shell
92shell$ echo -e "1 2 3 4 5 6 7 8 \n9" | egrep -o "[0-9]" | wc -l
9
```
######打印匹配字符串的行号
```shell
94shell$ echo -e "1 2 3 4 5 6 7 8 \n9" | egrep -o -n "[0-9]" 
1:1
1:2
1:3
1:4
1:5
1:6
1:7
1:8
2:9
```
`如果涉及到多个文件，还会打印相关的文件名`

######显示匹配的字符串和所在该行的偏移量
```shell
95shell$ echo i am simon | grep -b -o "simon"
5:simon
```
`该偏移量是从文件起始或者文本起始位置开始计算的`选项-o和选项-b配合使用

######搜索多个文件并查找匹配的文本位于那个文件中
```shell
112shell$ find /usr/include/* -name "*.h" | xargs -n1 grep "#define container_of" -l
/usr/include/btrfs/kerncompat.h
```
上面命令查找/usr/include目录下，`“#define container_of”`定义在那个头文件。
`其中-L显示不匹配的文件列表`

####grep补充
######递归查找文件
```shell
116shell$  grep "#define container_of" /usr/include/ -l -R 
/usr/include/btrfs/kerncompat.h
```
-R 和-r是一样的，-R 和 -r轻松的实现了上面find命令递归查找文件的功能，更方便简单。
######忽略大小写
使用选项-i来忽略匹配模式的大小写。
######匹配多个模式
```shell
120shell$ echo -e "hello\ni am simon" | grep  -o -e "i" -e "simon"  
i
simon
```
上面使用了两个-e选项，来实现匹配两个模式。
除了上面使用两个-e选项外，还可以使用-f来使用文本中定义的匹配模式。
######grep包含和排除
```shell
122shell$ grep "fprintf" /usr/include/ -r --include *.{c,h}
/usr/include/X11/Xtrans/Xtrans.c:    fprintf(stderr, "Launchd socket fd: %d\n", xquartz_launchd_fd);
/usr/include/X11/Xtrans/Xtrans.c:            fprintf(stderr,"Got NULL while trying to Reopen launchd port\n");
```
通过使用选项--include *.{c,h}，指定了grep命令在`.c`,`.h`文件中查找
```shell
135shell$ grep "fprintf" /usr/include/ -r --exclude   "p*"  
/usr/include/gawkapi.h:		fprintf(stderr, #module ": version mismatch with gawk!\n"); \
/usr/include/gawkapi.h:		fprintf(stderr, "\tmy version (%d, %d), gawk version (%d, %d)\n", \
/usr/include/fcitx-utils/uthash.h:#define HASH_OOPS(...) do { fprintf(stderr, __VA_ARGS__); } while (0)
/usr/include/c++/4.9/cstdio:#undef fprintf
/usr/include/c++/4.9/cstdio:#undef vfprintf
/usr/include/c++/4.9/cstdio:  using ::fprintf;
/usr/include/c++/4.9/cstdio:  using ::vfprintf;
/usr/include/error.h:/* Print a message with `fprintf (stderr, FORMAT, ...)';
/usr/include/x86_64-linux-gnu/bits/stdio2.h:extern int __fprintf_chk (FILE *__restrict __stream, int __flag,
/usr/include/x86_64-linux-gnu/bits/stdio2.h:extern int __vfprintf_chk (FILE *__restrict __stream, int __flag,
/usr/include/x86_64-linux-gnu/bits/stdio2.h:fprintf (FILE *__restrict __stream, const char *__restrict __fmt, ...)
/usr/include/x86_64-linux-gnu/bits/stdio2.h:  return __fprintf_chk (__stream, __USE_FORTIFY_LEVEL - 1, __fmt,
/usr/include/x86_64-linux-gnu/bits/stdio2.h:#  define fprintf(stream, ...) \
/usr/include/x86_64-linux-gnu/bits/stdio2.h:  __fprintf_chk (stream, __USE_FORTIFY_LEVEL - 1, __VA_ARGS__)
/usr/include/x86_64-linux-gnu/bits/stdio2.h:  return __vfprintf_chk (stdout, __USE_FORTIFY_LEVEL - 1, __fmt, __ap);
/usr/include/x86_64-linux-gnu/bits/stdio2.h:vfprintf (FILE *__restrict __stream,
/usr/include/x86_64-linux-gnu/bits/stdio2.h:  return __vfprintf_chk (__stream, __USE_FORTIFY_LEVEL - 1, __fmt, __ap);
/usr/include/x86_64-linux-gnu/bits/stdio.h:  return vfprintf (stdout, __fmt, __arg);
/usr/include/x86_64-linux-gnu/bits/libio-ldbl.h:__LDBL_REDIR_DECL (_IO_vfprintf)
/usr/include/x86_64-linux-gnu/bits/stdio-ldbl.h:__LDBL_REDIR_DECL (fprintf)
/usr/include/x86_64-linux-gnu/bits/stdio-ldbl.h:__LDBL_REDIR_DECL (vfprintf)
/usr/include/x86_64-linux-gnu/bits/stdio-ldbl.h:__LDBL_REDIR_DECL (__fprintf_chk)
/usr/include/x86_64-linux-gnu/bits/stdio-ldbl.h:__LDBL_REDIR_DECL (__vfprintf_chk)
/usr/include/stdio.h:extern int fprintf (FILE *__restrict __stream,
/usr/include/stdio.h:extern int vfprintf (FILE *__restrict __s, const char *__restrict __format,
/usr/include/libio.h:extern int _IO_vfprintf (_IO_FILE *__restrict, const char *__restrict,
/usr/include/btrfs/kerncompat.h:		fprintf(stderr, "%s:%d: %s: Assertion `%s` failed.\n",
/usr/include/btrfs/kerncompat.h:		fprintf(stderr, "%s:%d: %s: Assertion failed.\n", filename,
/usr/include/btrfs/kerncompat.h:#define printk(fmt, args...) fprintf(stderr, fmt, ##args)
/usr/include/X11/Xtrans/Xtransint.h:    vfprintf(stderr, f, args);
/usr/include/X11/Xtrans/Xtrans.c:    fprintf(stderr, "Launchd socket fd: %d\n", xquartz_launchd_fd);
/usr/include/X11/Xtrans/Xtrans.c:            fprintf(stderr,"Got NULL while trying to Reopen launchd port\n");
```

使用选项--exclude   "p*" ，意思是排除所有以p开头的文件。
######要打印匹配某个结果之后的3行，使用 -A选项
```shell
$ seq 10 | grep 5 -A 3
5
6
7
8
```
######要打印匹配某个结果之前的3行，使用 -B选项

```shell
$ seq 10 | grep 5 -B 3
2
3
4
5
```
######要打印匹配某个结果之前以及之后的3行，使用-C选项

```shell
$ seq 10 | grep 5 -C 3
2
3
4
5
6
7
8
```
######如果有多个匹配，那么使用--作为各部分之间的定界符

```shell
$ echo -e "a\nb\nc\na\nb\nc" | grep a -A 1
a
b
--
a
b
```



